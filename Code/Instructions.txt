To test the codes follow the sequence:
1. Reset paths for accessing session ".csv" files in ClickStreamAnomalyDetection.R
2. Might need to install packages such as "markovchain", "seqHMM" and "TraMineR" using install.packages("package_name")
3. Run ClickStreamAnomalyDetection.R
4. Run Visualization.R

